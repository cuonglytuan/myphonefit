package com.example.phonefit;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.phonefit.R;
import com.google.android.gcm.GCMRegistrar;

public class RegisterActivity extends Activity implements OnClickListener {

	private EditText edtUsername;
	private EditText edtEmail;
	private EditText edtPassword;
	private Button btnRegister;
	private ProgressDialog pDialog;

	private String tag_json_obj = "jobj_req";
	private static final int REGISTER_SUCCESS = 0;
	private static final int LOGIN_FAILED = 1;
	private Context mContext;
	private final String TAG = "RegisterScreen";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		initView();
	}

	private void initView() {
		mContext = this;

		if (edtUsername == null) {
			edtUsername = (EditText) findViewById(R.id.activity_register_edt_username);
		}

		if (edtEmail == null) {
			edtEmail = (EditText) findViewById(R.id.activity_register_edt_email);
		}

		if (edtPassword == null) {
			edtPassword = (EditText) findViewById(R.id.activity_register_edt_password);
		}

		if (btnRegister == null) {
			btnRegister = (Button) findViewById(R.id.activity_register_btn_register);
		}

		pDialog = new ProgressDialog(this);
		pDialog.setMessage("Loading...");
		pDialog.setCancelable(false);

		listener();
	}

	private void listener() {
		btnRegister.setOnClickListener(this);
	}
	
	void showProgressDialog() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (pDialog == null) {
					pDialog = new ProgressDialog(RegisterActivity.this);
					pDialog.setMessage("Loading...");
					pDialog.setCancelable(true);
					pDialog.show();
				} else {
					pDialog.show();
				}
			}
		});
	}

	void dismissProgressDialog() {
		if (pDialog != null && pDialog.isShowing()) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					pDialog.dismiss();
				}
			});
		}
	}

	private void actionRegister(final String userName, final String password,
			final String email) {
		Log.i("tuancuong", "regId: " + GCMIntentService.regId);
		showProgressDialog();
		String url = "https://phone-fit.appspot.com/_ah/api/account/v1/account/created";

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("deviceid", GCMIntentService.regId);
			jsonObject.put("email", email);
			jsonObject.put("password", password);
			jsonObject.put("username", userName);

		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST, url,
				jsonObject, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						dismissProgressDialog();
						String status = response.optString("status", null);
						if (status.contains("OK")) {
							mHandler.sendEmptyMessage(REGISTER_SUCCESS);
						} else {
							dismissProgressDialog();
							mHandler.sendEmptyMessage(LOGIN_FAILED);
						}
						Log.d(TAG, response.toString());
						// pDialog.hide();
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						dismissProgressDialog();
						VolleyLog.d(TAG, "Error: " + error.getMessage());
					}
				}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				return headers;
			}

		};

		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

		// Cancelling request
		// ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);
	}

	private Handler mHandler = new Handler(new Handler.Callback() {

		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
				case REGISTER_SUCCESS :
					Intent intentRegister = new Intent();
					intentRegister.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intentRegister.setClass(mContext, MainActivity.class);
					startActivity(intentRegister);
					finish();
					break;

				case LOGIN_FAILED :
					Toast.makeText(mContext, "Register is not success",
							Toast.LENGTH_SHORT).show();
					break;

				default :
					break;
			}
			return false;
		}
	});

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.activity_register_btn_register :
				String userName = edtUsername.getText().toString().trim();
				String passWord = edtPassword.getText().toString().trim();
				String email = edtEmail.getText().toString().trim();
				actionRegister(userName, passWord, email);
				break;

			default :
				break;
		}
	}
}
