package com.example.phonefit;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.phonefit.R;
import com.google.android.gcm.GCMRegistrar;

public class LoginActivity extends Activity implements OnClickListener {

	private EditText edtUsername;
	private EditText edtPassword;
	private Button btnLogin;
	private Button btnRegister;
	private Context mContext;

	private ProgressDialog pDialog;
	private String tag_json_obj = "jobj_login_req";
	private static final int LOGIN_SUCCESS = 0;
	private static final int LOGIN_FAILED = 1;
	private final String TAG = "RegisterScreen";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		initView();
	}

	private void initView() {

		mContext = this;

		if (edtUsername == null) {
			edtUsername = (EditText) findViewById(R.id.activity_login_edt_username);
		}

		if (edtPassword == null) {
			edtPassword = (EditText) findViewById(R.id.activity_login_edt_password);
		}

		if (btnLogin == null) {
			btnLogin = (Button) findViewById(R.id.activity_login_btn_login);
		}

		if (btnRegister == null) {
			btnRegister = (Button) findViewById(R.id.activity_login_btn_register);
		}

		pDialog = new ProgressDialog(this);
		pDialog.setMessage("Loading...");
		pDialog.setCancelable(false);

		listener();
	}

	private void listener() {
		btnLogin.setOnClickListener(this);
		btnRegister.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.activity_login_btn_login :
				String userName = edtUsername.getText().toString().trim();
				String passWord = edtPassword.getText().toString().trim();
				actionLogin(userName, passWord);
				break;

			case R.id.activity_login_btn_register :
				Intent intentRegister = new Intent();
				intentRegister.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intentRegister.setClass(mContext, RegisterActivity.class);
				startActivity(intentRegister);
				finish();
				break;

			default :
				break;
		}
	}

	private void showProgressDialog() {
		if (!pDialog.isShowing())
			pDialog.show();
	}

	private void hideProgressDialog() {
		if (pDialog.isShowing())
			pDialog.hide();
	}

	private void actionLogin(final String userName, final String password) {
		showProgressDialog();
		String url = "https://phone-fit.appspot.com/_ah/api/account/v1/account/login";

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("email", userName);
			jsonObject.put("password", password);

		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST, url,
				jsonObject, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						hideProgressDialog();
						String status = response.optString("status", null);
						if (status.contains("OK")) {
							mHandler.sendEmptyMessage(LOGIN_SUCCESS);
						} else {
							hideProgressDialog();
							mHandler.sendEmptyMessage(LOGIN_FAILED);
						}
						Log.d(TAG, response.toString());
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						hideProgressDialog();
						VolleyLog.d(TAG, "Error: " + error.getMessage());
					}
				}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				return headers;
			}

		};

		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

		// Cancelling request
		// ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);
	}

	private Handler mHandler = new Handler(new Handler.Callback() {

		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
				case LOGIN_SUCCESS :
					Intent intentRegister = new Intent();
					intentRegister.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intentRegister.setClass(mContext, MainActivity.class);
					startActivity(intentRegister);
					finish();
					break;

				case LOGIN_FAILED :
					Toast.makeText(mContext,
							"Please check your username and password!",
							Toast.LENGTH_SHORT).show();
					break;

				default :
					break;
			}
			return false;
		}
	});
}
